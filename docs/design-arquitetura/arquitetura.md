# Arquitetura do SIGE

O SIGE é composto por 4 camadas:
1. Servidor de Coleta de Dados
2. Servidor Central
3. Servidor Web (Frontend Web)
4. App Mobile
   
![Arquitetura](../assets/images/arquitetura_sige.png)